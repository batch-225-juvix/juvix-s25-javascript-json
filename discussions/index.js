// JSON or JavaScript Object Notation, is a data format used by applications to store and transport data to one another. Or to transfer data files.

// Files that store JSON data are saved with a file extension of .json

// magkaiba un sir G... pag .js more of programming logic... pag .json isipin mo storage/file type.

// bawal mag,lagay ng comment tag sa package.json

/*
Why JSON?

below is an example of a JSON

{
	"to": "Tove",
	"from": "Jani",
	"heading": "Reminder",
	"body": "Don't forget me this weekend!touch

}

/*--------------------------------*/


// I.
// [SECTION] JSON Objects
/*
	- JSON stands for Javascript Object Notation.
	- JSON is also used in other programming languages hence the name Javascript Object Notation.

	SYNTAX:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}

*/


// JSON Objects
// for example:

/*

{

	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}


*/
// note: We cannot put JSON objects in javascript, instead we put this json syntax in package.json file





// II.
// [SECTION] JSON Arrays
/*
"cities": [
    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
]
*/





// III.
// [SECTION] JSON METHODS
/*
	- the JSON object contains methods for parsing and converting data into stringified JSON.
*/


// [SECTION] Converting Data into Stringified JSON
/*
	- Stringified JSON is Javascript object converted into string to be used in other functions of a javascript application.
	- They are commonly used in HTTP request where information is required to be send and recieved in a stringfied JSON format.
	- Request are an important part of programming where application communicates with a backend application to perform different tasks such as getting/creating data in a database.


// array convert into json stringified.
// {batchName: 'Batch X'} means value
// Q: kasamang naprint yung mga curly braces sir.
// Ans: Yes, makikita natin yan sa console sa google chrome.
*/

// for example:
let batchArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'},];

// The "stringify" method is used to convert JavaScript Objects into a string.
console.log('Results from stringify method:');
console.log(JSON.stringify(batchArr));


// ANOTHER EXAMPLE:
// Through Objects stringify
let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);


// [SECTION] Using Stringify Method with Variables.
/*
	JSON.stringify({
		propertyA: variableA,
		propertyB: variableB
	})



// inside of ({}) is we can create a property.
	such as ({
		propertyA: variableA,
		propertyB: variableB
	})
*/


// User details
/*

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address,
})

console.log(otherData);

*/


/*---------------------------------------*/
// III.
// [Section] Converting stringified JSON into JavaScript objects
/*
	- Objects are common data types used in applications because of the complex data structures that can be created out of them.

	- Information is commonly sent to applications in stringified JSON and then converted back into objects.

	- This happens both for sending information to a backend application and sending information back to a frontend application.
	
	- Parsing means analyzing and converting a program into an internal format that a runtime environment can actually run

	- JSON.parse()


// stringify into object.
*/

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`

console.log(JSON.parse(stringifiedObject));


// between client and server
// JSON is always converted into Javascript Object
// kailangan natin c JSON.parse para makapasok sa server.
// sa loob ng server is gagamit tayo ng JSON.stringify 
// JSON(String) -> JS Object
// JSON to JS object
// Json.parse ->Server->Json.stringify->json object 

// From JSON String to JS Object = parse  - client -> server

// From JS Objects to JSON String = Stringify server -> client

// Q: yung buong code po natin sa index.js ipaparse natin para mastore sa server? then para magappear siya sa mga devices, need istringify?
// Ans: YES

// JSON ung ginagamit na passing ng information kasi madali siya ipasa kasi free text lang sya... unlike kung object ipapasa mo kelangan magkasundo sundo lahat ng web kung ano itsura ng object.

